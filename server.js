const fs = require('fs')
const http = require('http')
const { v4: uuidv4 } = require('uuid')

const readFile = (fileName) => {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, "utf-8", (error, data) => {
            if (!error) {
                resolve(data)
            }
            else {
                reject(error)
            }
        })

    })
}

const throwError = (request, response, error) => {

    response.writeHead(500, {
        "Content-Type": "application/json"
    })
    response.write(JSON.stringify({
        message: "There is an error from the server side."
    }))
    response.end()
}

const server = http.createServer((request, response) => {

    switch (request.url) {

        case '/':
            {
                readFile("./index.html")
                    .then((data) => {
                        response.writeHead(200, { "Content-Type": "text/html" })
                        response.write(data)
                        response.end()
                    }).catch((error) => {
                        throwError(request, response, error)
                    })
                break
            }
        case '/html':
            {
                readFile("./index.html")
                    .then((data) => {
                        response.writeHead(200, { "Content-Type": "text/html" })
                        response.write(data)
                        response.end()
                    }).catch((error) => {
                        throwError(request, response, error)
                    })
                break
            }
        case '/json':
            {
                readFile("./app.json")
                    .then((data) => {
                        response.writeHead(200, { "Content-Type": "application/json" })
                        response.write(data)
                        response.end()
                    }).catch((error) => {
                        throwError(request, response, error)
                    })
                break
            }
        case '/uuid':
            {
                try {
                    let data = (uuidv4())
                    let uniqueID = data
                    let uuidObject = {}
                    uuidObject["uuid"] = uniqueID
                    response.write(JSON.stringify(uuidObject))
                    response.end()
                }
                catch (error) {
                    throwError(request, response, error)
                }
                break
            }

        default: {

            try {

                let splitUrl = request.url.split("/")
                let urlResponse = "/status/" + splitUrl[2]
                if (request.url === urlResponse) {
                    response.write(splitUrl[2])
                    response.end()
                    break

                }
            } catch (error) {
                throwError(request, response, error)
                break
            }
            try {
                let splitUrl = request.url.split("/")
                let seconds = parseInt(splitUrl[2])
                let delay = "/delay/" + splitUrl[2]
                if (request.url === delay) {

                    setTimeout(() => {
                        response.write("200")
                        response.end()
                    }, seconds * 1000)
                    break
                }

            }
            catch (error) {
                throwError(request, response, error)
                break
            }
            response.writeHead(404, {
                "Content-Type": "text/html",
            })
            response.write("OOPS error !!! 404 not found")
            response.end()
            break

        }
    }
})

server.listen(3030)